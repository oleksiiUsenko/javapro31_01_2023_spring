package edu.hillel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests()
        .antMatchers(HttpMethod.DELETE, "/orders/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/orders/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.DELETE, "/products/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/products/**").hasRole("ADMIN")
        .and().httpBasic();
    http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS); // Не використовувати сесії
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
