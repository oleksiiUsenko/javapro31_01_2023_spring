package edu.hillel.mapper;

import edu.hillel.dto.UserDto;
import edu.hillel.entity.User;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
  User toEntity(UserDto dto);

  UserDto toDto(User entity);

  List<User> toEntity(List<UserDto> dtos);

  List<UserDto> toDto(List<User> entities);
}
