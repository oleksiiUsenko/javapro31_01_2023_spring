package edu.hillel.mapper;

import edu.hillel.dto.OrderDto;
import edu.hillel.entity.Order;
import java.util.List;
import org.mapstruct.Mapper;




@Mapper(componentModel = "spring")
public interface OrderMapper {
  Order toEntity(OrderDto dto);

  OrderDto toDto(Order entity);

  List<Order> toEntity(List<OrderDto> dtos);

  List<OrderDto> toDto(List<Order> entities);
}
