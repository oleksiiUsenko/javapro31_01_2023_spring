package edu.hillel.mapper;


import edu.hillel.dto.ProductDto;
import edu.hillel.entity.Product;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
  Product toEntity(ProductDto dto);

  ProductDto toDto(Product entity);

  List<Product> toEntity(List<ProductDto> dtos);

  List<ProductDto> toDto(List<Product> entities);
}