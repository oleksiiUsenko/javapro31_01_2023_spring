package edu.hillel.service;

import edu.hillel.dto.UserDto;
import edu.hillel.mapper.UserMapper;
import edu.hillel.repo.UserRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;

  @Override
  public UserDetails loadUserByUsername(String username) {
    log.info("Getting UserDetails by username: {}", username);
    UserDto userDto = userMapper.toDto(userRepository.findByUsername(username));
    return User
        .withUsername(userDto.getUsername())
        .password(userDto.getPassword())
        .roles(userDto.getRole())
        .build();
  }
}
