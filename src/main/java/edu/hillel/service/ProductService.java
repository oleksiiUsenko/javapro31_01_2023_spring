package edu.hillel.service;

import edu.hillel.dto.ProductDto;
import edu.hillel.mapper.ProductMapper;
import edu.hillel.repo.ProductRepository;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class ProductService {
  private final ProductRepository productRepository;
  private final ProductMapper productMapper;

  public ProductDto addProduct(ProductDto productDto) {
    log.info("Adding product: {}", productDto);
    return productMapper.toDto(productRepository.save(productMapper.toEntity(productDto)));
  }

  public ProductDto getProductById(int id) {
    log.info("Getting product by id: {}", id);
    return productMapper.toDto(productRepository.getReferenceById(id));
  }

  public List<ProductDto> getAllProducts() {
    log.info("Getting all products");
    return productRepository
        .findAll()
        .stream()
        .map(productMapper::toDto)
        .collect(Collectors.toList());
  }

  public void delete(int id) {
    log.info("Deleting product by Id: {}", id);
    productRepository.deleteById(id);
  }

}
