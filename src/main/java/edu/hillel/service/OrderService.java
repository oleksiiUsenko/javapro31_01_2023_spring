package edu.hillel.service;

import edu.hillel.dto.OrderDto;
import edu.hillel.mapper.OrderMapper;
import edu.hillel.repo.OrderRepository;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
@Transactional
public class OrderService {
  private final OrderRepository orderRepository;
  private final OrderMapper orderMapper;

  public OrderDto addOrder(OrderDto orderDto) {
    log.info("Adding order: {}", orderDto);
    return orderMapper.toDto(orderRepository.save(orderMapper.toEntity(orderDto)));
  }

  public OrderDto getOrderById(int id) {
    log.info("Getting order by id: {}", id);
    return orderMapper.toDto(orderRepository.getReferenceById(id));
  }

  public List<OrderDto> getAllOrders() {
    log.info("Getting all orders");
    return orderRepository
        .findAll()
        .stream()
        .map(orderMapper::toDto)
        .collect(Collectors.toList());
  }

  public void delete(int id) {
    log.info("Deleting order by Id: {}", id);
    orderRepository.deleteById(id);
  }
}