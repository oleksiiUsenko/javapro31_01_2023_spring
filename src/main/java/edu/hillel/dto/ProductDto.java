package edu.hillel.dto;


import lombok.Data;

@Data
public class ProductDto {
  int id;
  String name;
  double cost;
}
