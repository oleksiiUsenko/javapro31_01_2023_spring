package edu.hillel.dto;

import lombok.Data;

@Data
public class UserDto {
  int id;
  String username;
  String password;
  String role;
}
