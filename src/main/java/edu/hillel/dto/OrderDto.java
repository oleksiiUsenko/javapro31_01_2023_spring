package edu.hillel.dto;

import edu.hillel.entity.Product;
import java.util.Date;
import java.util.List;
import lombok.Data;

@Data
public class OrderDto {

  int id;
  Date date = new Date();
  double cost;
  List<Product> products;
}