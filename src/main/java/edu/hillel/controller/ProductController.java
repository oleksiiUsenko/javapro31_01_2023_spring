package edu.hillel.controller;

import edu.hillel.dto.ProductDto;
import edu.hillel.service.ProductService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
  private final ProductService productService;

  @PostMapping
  public ResponseEntity<ProductDto> add(@RequestBody ProductDto productDto) {
    return ResponseEntity.ok(productService.addProduct(productDto));
  }

  @GetMapping("/{id}")
  public ResponseEntity<ProductDto> getOrderById(@PathVariable int id) {
    return ResponseEntity.ok(productService.getProductById(id));
  }

  @GetMapping
  public ResponseEntity<List<ProductDto>> getAllOrders() {
    return ResponseEntity.ok(productService.getAllProducts());
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable("id") int id) {
    productService.delete(id);
    return ResponseEntity.ok().build();
  }
}