CREATE DATABASE homework34;
USE homework34;


CREATE TABLE orders (
  id INT PRIMARY KEY AUTO_INCREMENT,
  date DATETIME,
  cost DECIMAL(10, 2) DEFAULT 0
);

CREATE TABLE products (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  cost DECIMAL(10, 2)
);

CREATE TABLE order_products (
  order_id INT,
  product_id INT,
  PRIMARY KEY (order_id, product_id),
  FOREIGN KEY (order_id) REFERENCES orders(id),
  FOREIGN KEY (product_id) REFERENCES products(id)
);

CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  role VARCHAR(255)
);


CREATE USER "username"@"localhost" IDENTIFIED BY "password";
GRANT CREATE, REFERENCES, SELECT, UPDATE, DELETE, INSERT, DROP ON orders TO "username"@"localhost";
GRANT CREATE, REFERENCES, SELECT, UPDATE, DELETE, INSERT, DROP ON products TO "username"@"localhost";
GRANT CREATE, REFERENCES, SELECT, UPDATE, DELETE, INSERT, DROP ON order_products TO "username"@"localhost";
GRANT CREATE, REFERENCES, SELECT, UPDATE, DELETE, INSERT, DROP ON users TO "username"@"localhost";

