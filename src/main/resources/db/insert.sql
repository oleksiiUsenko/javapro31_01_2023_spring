INSERT INTO orders (date, cost) VALUES ('2023-01-12', 2000.0);
INSERT INTO products (name, cost) VALUES ('Product 1', 10.0);
INSERT INTO order_products (order_id, product_id) VALUES (1, 1);
INSERT INTO users (username,password,role) VALUES ('admin','$2y$10$bdM.ngsTDGpXaUmQnUrLOOAGK/uR2sl7b1pVXPcsgggZX76pAW.8i','ADMIN');